# Plandroid

## What is it?

It is a small app that allows the user to quickly and effortless set quick notes and reminders for tasks. Anything you could want to remember can be put down, and the app will figure out what kind of note to display. 

Say you just want to remember something quick, just give a title. Or add a description for more detail. Or maybe you have a meeting, as a location and a time for the alert. 
Have to do something each week, not trouble choose when it repeats and how often.


## Why is it?

It was made for a project in second year in univeristy. 

## Why would I want this. 
Its better to have someone else or something else remember everything for you. Offload your mind to more important decisions. I hear its why people have assistants. 